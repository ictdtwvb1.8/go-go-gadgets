package retourprocess;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class Order {
    final private int _orderid;
    final private LocalDate _date;
    final private static Map<Integer, Order> _orders = new HashMap<>();

    public Order(int orderid, String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        _orderid = orderid;
        _date = LocalDate.parse(date, formatter);
    }

    public void setOrder(int orderid, Order order) {
        _orders.put(orderid, order);
    }

    public static Order getOrder(int orderid) {
        return _orders.get(orderid);
    }

    @Override
    public String toString() {
        return _orderid + ", " + _date;
    }
}
