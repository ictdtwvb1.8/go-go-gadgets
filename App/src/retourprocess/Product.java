package retourprocess;

import java.awt.image.BufferedImage;

public class Product {
    private int _productid;
    private String _name;
    private BufferedImage _photo;

    public int getProductCode() {
        return _productid;
    }

    public String getName() {
        return _name;
    }

    public BufferedImage getPhoto() {
        return _photo;
    }
}
