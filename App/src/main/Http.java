package main;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Http {
    private final HttpClient _client;

    public Http() {
        _client = HttpClient.newHttpClient();
    }

    /*
        met deze methode kan je een http request uitvoeren
     */
    public HttpResponse<String> Request(String uri){
        try {
            // request wordt hier aangemaakt
            final HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .header("Accept", "application/json")
                    .build();

            // de request wordt hier verzonden naar het adres
            return _client.send(request, HttpResponse.BodyHandlers.ofString());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return null;
    }

}
