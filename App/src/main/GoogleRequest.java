package main;

import Models.Destination;
import Models.Route;
import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.net.URLEncoder;
import java.net.http.HttpResponse;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
 /*
    GoogleRequest zorgt er voor dat de data van de Google API afgehaald wordt.
    de return waardes zijn bestemmingen op volgorde.
 */
public class GoogleRequest {
    private final static Map<Integer, Destination> _to = new HashMap<>();
    private final static Map<Integer, Destination> _from = new HashMap<>();
    private final static Map<String, Integer> _toMap = new HashMap<>();
    private final static Map<String, Integer> _fromMap = new HashMap<>();
    private static Map<String, Destination> _destinations = new HashMap<>();
    private final String _APIKEY = "AIzaSyAJFS2bUV8gPVwRDVVWqPPpx8xKPBCS4xY";
    private final String _originId;
    private final String _origin;
    private final Map<String, String> _destinationAddresses = new HashMap<>();


    //bestemmingen berekenen op basis van start (school)
    public GoogleRequest(Map<String, Destination> destinations){
        _originId       = "Start";
        _origin         = "Campus 2 Zwolle";

        for(var destination : destinations.entrySet()){
            try {
                //adressen samen voegen in een map voor latere gebruik
                _destinationAddresses.put(destination.getKey() ,destination.getValue().getAddress());
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    //bestemmingen berekenen vanaf bestemmingpunt
    public GoogleRequest(Destination origin, Map<String, Destination> destinations){
        _originId       = origin.getId();
        _origin         = origin.getAddress();

        for(var destination : destinations.entrySet()){
            try {
                //adressen samen voegen in een map voor latere gebruik
                _destinationAddresses.put(destination.getKey() ,destination.getValue().getAddress());
            }
            catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    /*
        deze methode zorgt ervoor dat de route op de beste punt in volgorde komt te staan.
     */
    private static void setRoute(int destinationSize, String key, Map<String, Integer> distanceMap) throws Exception {
        //distanceMap van de startpunt
        _toMap.putAll(distanceMap);
        //welke map voor het laatst is geupdate
        boolean from = true;

        //while loop om te zorgen dat het doorgaat tot dat het voldoet aan de aantal bestemmingen.
        while ((_to.size() + _from.size()) <= destinationSize){
            Destination nextDestination = _destinations.get(key);
            _destinations.remove(key);

            if(_destinations.size() == 0){
                break;
            }

            //Google API Call
            final HttpResponse<String> response = new GoogleRequest(nextDestination, _destinations)
                    .request();

            try {
                //voorkomt timeout van de Google API
                TimeUnit.MICROSECONDS.sleep(200);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }

            final JSONObject jsonObject = formatBodyToJsonObject(response);
            distanceMap = getDistancesOfJsonObject(jsonObject);
            final Destination destination = getShortestRouteFrom(distanceMap);
            key = destination.getId();

            // als de terugweg 1 bestemming krijgt volgt er een andere berekening
            if(_fromMap.size() > 0){
                if(from){
                    if(distanceMap.get(key) < _toMap.get(key)){
                        _from.put((destinationSize-1) - _from.size(), destination);
                        _fromMap.putAll(distanceMap);
                        from = true;
                    }
                    else {
                        _to.put(_to.size(), destination);
                        _toMap.putAll(distanceMap);
                        from = false;
                    }
                }
                else {
                    if(distanceMap.get(key) < _fromMap.get(key)){
                        _to.put(_to.size(), destination);
                        _toMap.putAll(distanceMap);
                        from = false;
                    }
                    else {
                        _from.put((destinationSize-1) - _from.size(), destination);
                        _fromMap.putAll(distanceMap);
                        from = true;
                    }
                }

            }
            else {
                // de eerste bestemming van de terugweg word pas toegevoegd als de bestemming 25km van de vorige bestemming lag.
                if(destination.getDistanceFromDestionation() <= 25000){
                    _to.put(_to.size(), destination);
                    _toMap.putAll(distanceMap);
                }
                else {
                    _from.put((destinationSize-1) - _from.size(), destination);
                    _fromMap.putAll(distanceMap);
                }
            }
        }

    }
    /*
        deze methode pakt de dichtstbijzijnde bestemming
     */
    private static Destination getShortestRouteFrom(Map<String, Integer> map){
        String mapkey = null;
        int closed = -1;

        for(var dest : map.entrySet()){
            final String currentKey = dest.getKey();
            final int distance = dest.getValue();

            // hij kijkt eerst of er uberhauth een key gezet is, anders kijkt hij of de afstand kleiner is dan de vorige cicle.
            if(mapkey == null || distance <= closed){
                mapkey = currentKey;
                closed = distance;

                // als de bestemming 0 meter van de vorige ligt, stopt de for loop.
                if(distance == 0)
                    break;

            }
        }

        // haalt de bestemming op
        Destination destination = _destinations.get(mapkey);

        if(destination == null){
            // dubbel check
            return null;
        }

        // update de bestemming object
        destination.updateDistance(closed);
        _destinations.put(mapkey, destination);
        destination = _destinations.get(mapkey);

        return destination;
    }

    /*
        maakt van een JSON string een JSONObject
     */
    private static JSONObject formatBodyToJsonObject(HttpResponse response) {
        final JSONParser parse  = new JSONParser();
        final String body       = (String) response.body();
        JSONObject jsonObject         = null;

        try {
            jsonObject = (JSONObject) parse.parse(body);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    /*
        deze methode zorgt ervoor dat van een JsonObject een Distance object wordt gemaakt.
     */
    private static Map<String, Integer> getDistancesOfJsonObject(JSONObject jsonObject) throws Exception {
        final String json = new Gson().toJson(jsonObject);
        final Route route = new Gson().fromJson(json, Route.class);
        final Map<Integer, Integer> map = route.getDistances();
        final Map<String, Integer> distanceMap = new HashMap<>();

        // check van de limit van 25 is bereikt
        if(route.getStatus().equals("MAX_DIMENSIONS_EXCEEDED")){
            throw new Exception("Exception message");
        }

        for(var dist : map.entrySet()){
            final int index = dist.getKey();
            final int distance = dist.getValue();
            int count = 0;

            for(var dest : _destinations.entrySet()){
                final String key = dest.getKey();

                if(index == count){
                    distanceMap.put(key, distance);
                    break;
                }

                count++;
            }
        }

        return distanceMap;
    }

    /*
        deze functie maakt van een string een url string, wat geldig is voor de browser
     */
    private static String encodeValue(String value) {
        return URLEncoder.encode(value);
    }

    /*
        deze methode haal ik de route op op basis van een datum
     */
    public static Map<Integer, Destination> getMap(String date) {
        // de bestemming worden gezet
        Destination.setDestinations(date);
        // de bestemmingen worden opgehaald
        _destinations = Destination.getDestinations();
        // aantal bestemmingen van die dag
        final int destinationSize = _destinations.size();
        // er wordt een API call gedaan naar Google met de start bestemming
        final HttpResponse<String> response = new GoogleRequest(_destinations)
                .request();

        // van de response van de Google API call wordt een JsonObject gegenereerd
        final JSONObject jsonObject = formatBodyToJsonObject(response);

        try {
            // van het JsonObject worden de afstanden gemaped naar id => afstand
            final Map<String, Integer> distancesMap = getDistancesOfJsonObject(jsonObject);
            // van het gemaped afstanden wordt er een bestemming object gegenereerd
            final Destination destination = getShortestRouteFrom(distancesMap);
            // id/id's van de bestemming(en)
            final String key = destination.getId();

            _to.put(_to.size(), destination);

            // hier wordt de route berekend
            setRoute(destinationSize, key, distancesMap);

            // samenvoegen van de heen en terugweg
            _from.forEach(_to::put);

            return _to;
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /*
        Het aanmaken van een request naar Google API
     */
    public HttpResponse<String> request () {
        final int size = _destinationAddresses.size();
        int index = 0;
        StringBuilder destinationURL = new StringBuilder();

        // alle adressen van de bestemmingen worden hier bijelkaar gevoegd
        for(var d : _destinationAddresses.entrySet()){
            final String address = d.getValue();
            final String separator = "|";

            if(index >= size){
                destinationURL.append(address);
            }
            else {
                destinationURL.append(separator);
                destinationURL.append(address);
            }

            index++;
        }

        // url string wordt hier opgebouwd
        String url = MessageFormat.format(
                "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}",
                encodeValue(_origin),
                encodeValue(destinationURL.toString()),
                _APIKEY
        );

        try {
            // http request wordt hier gedaan met de gegenereerde url
            final Http http = new Http();
            return http.Request(url);
        }
        catch (NullPointerException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getOrigin(){
        return _origin;
    }

    /*
        haalt een bestemming uit de lijst met bestemmingen
     */
    public static void removeMapById(String id){
        _destinations.remove(id);
    }

    /*
        leegt de statische waardes
     */
    public static void clear(){
        _to.clear();
        _from.clear();
        _toMap.clear();
        _fromMap.clear();
        _destinations.clear();
    }
}
