package GUI;

import javax.swing.*;
import java.awt.*;

public class Button {
    private JButton _btn;

    public Button(String btnText) {
        _btn = new JButton(btnText);

        _btn.setBorderPainted(false); // Removes border
        _btn.setFocusPainted(false); // Removes focus style
        _btn.setFont(GUI.getArialBold().deriveFont(12f)); // Sets font and font size
        _btn.setForeground(GUI.getWhite()); // Sets font color
        _btn.setBackground(GUI.getBlue()); // Sets background color
        _btn.setMargin(new Insets(5, 15, 5, 15)); // Sets top and bottom space
        _btn.setCursor(new Cursor(Cursor.HAND_CURSOR)); // Makes cursor a pointer
    }

    public JButton getButton() {
        return _btn;
    }
}
