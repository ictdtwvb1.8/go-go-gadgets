package GUI;

import dbconnection.dbconnection;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HomeGUI implements ActionListener {
    final private JPanel jpHomeContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    final private JScrollPane _scrollPanel = new JScrollPane(jpHomeContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    private JPanel jpCustomer;
    private JLabel jlName;
    private JLabel jlStreet;
    private JLabel jlPostal;
    private JLabel jlProvince;
    private JButton jbChoose;

    public HomeGUI() throws SQLException {
        _scrollPanel.setPreferredSize(new Dimension(750, 550));
        _scrollPanel.setBorder(new EmptyBorder(0, 0,5,0));

        jpHomeContainer.setBackground(GUI.getWhite());
        jpHomeContainer.setBorder(new EmptyBorder(30, 30, 30, 30));

        dbconnection connection = new dbconnection();

        ResultSet customersData = connection.executeQuery(" select customerid, Customername, addressline1, postalcode, cityname from wideworldimporters.v_ng_customers");
        while(customersData.next()) {
            int customerid = customersData.getInt("customerid");
            String Customername = customersData.getString("Customername");
            String addressline1 = customersData.getString("addressline1");
            String postalcode = customersData.getString("postalcode");
            String cityname = customersData.getString("cityname");

            jpHomeContainer.add(setCustomer(customerid,Customername,addressline1,postalcode,cityname));
        }
        connection.closeConnection();

        int count = (jpHomeContainer.getComponentCount() % 3);

        if (count > 0) {
            count = (jpHomeContainer.getComponentCount() / 3) + 1;
        } else {
            count = (jpHomeContainer.getComponentCount() / 3);
        }

        jpHomeContainer.setPreferredSize(new Dimension(750, count * 180 + 30));
    }

    public JScrollPane getHome() {
        return _scrollPanel;
    }

    public JPanel setCustomer(int customerid, String name, String street, String postal, String province) {
        JPanel jpCustomerSpacer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        jpCustomer = new JPanel();

        jpCustomerSpacer.setBackground(GUI.getWhite());
        jpCustomerSpacer.setBorder(new EmptyBorder(0, 0, 30, 30));

        jpCustomer.setPreferredSize(new Dimension(200, 150));
        jpCustomer.setLayout(new BoxLayout(jpCustomer, BoxLayout.Y_AXIS));
        jpCustomer.setBackground(GUI.getWhite());
        jpCustomer.setBorder(new CompoundBorder(
                BorderFactory.createLineBorder(GUI.getBlue()),
                new EmptyBorder(10, 10, 10, 10)));

        jlName = new JLabel(name);
        jlStreet = new JLabel(street);
        jlPostal = new JLabel(postal);
        jlProvince = new JLabel(province);

        jlName.setFont(GUI.getArialBold().deriveFont(16f));
        jlStreet.setFont(GUI.getArial());
        jlPostal.setFont(GUI.getArial());
        jlProvince.setFont(GUI.getArial());

        Button choose = new Button("Kies klant");
        jbChoose = choose.getButton();
        jbChoose.setName(String.valueOf(customerid));

        jbChoose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUI.setCustomerID(Integer.parseInt(((JComponent) e.getSource()).getName()));

                try {
                    OrdersGUI.getOrders();
                    RetoursGUI.getRetours();
                    RetourRegistrationGUI.getRegistrationRetours();
                    ProductRegistrationGUI.getRegistrationRetourProducts();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

                GUI.login();
            }
        });

        jlName.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlStreet.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlPostal.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlProvince.setAlignmentX(Component.CENTER_ALIGNMENT);
        jbChoose.setAlignmentX(Component.CENTER_ALIGNMENT);

        jpCustomer.add(jlName);
        jpCustomer.add(Box.createVerticalStrut(10));
        jpCustomer.add(jlStreet);
        jpCustomer.add(jlPostal);
        jpCustomer.add(jlProvince);
        jpCustomer.add(Box.createVerticalStrut(10));
        jpCustomer.add(jbChoose);

        jpCustomerSpacer.add(jpCustomer);

        return jpCustomerSpacer;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
