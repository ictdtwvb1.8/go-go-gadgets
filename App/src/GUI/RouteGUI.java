package GUI;

import Models.Destination;
import dbconnection.dbconnection;
import main.GoogleRequest;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RouteGUI implements ActionListener {
    final private JPanel _jpRouteScrollContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

    final private JPanel jpRoute = new JPanel(new GridBagLayout());
    final private JScrollPane _scrollPanel = new JScrollPane(jpRoute, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    final private JPanel jpSelectAddressesContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    final private JPanel addressContainer = new JPanel();
    final private JPanel jpCalculateContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

    final private JPanel jpFinalRouteContainer = new JPanel(new GridBagLayout());
    final private JScrollPane _finalScrollPanel = new JScrollPane(jpFinalRouteContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    final private JPanel jpOrderedAddressesContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
    final private JPanel jpNewCalculateContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

    final private JComboBox jcbAddressDropdown;
    final private JButton jpRetrieveAdresses;
    final private JLabel jlAddressesLabel = new JLabel("Adressen");
    final private JButton jbCalculate;

    final private JLabel jlCalcRouteLabel = new JLabel("Berekende route");
    final private JButton jbNewCalculate;

    JPanel routeAddresses = new JPanel();

    public RouteGUI() throws SQLException {
        _scrollPanel.setPreferredSize(new Dimension(755, 500));
        _scrollPanel.setBorder(new EmptyBorder(0, 0,7,0));

        jpRoute.setBackground(GUI.getWhite());
        jpRoute.setBorder(new EmptyBorder(30, 30, 30, 30));

        _finalScrollPanel.setPreferredSize(new Dimension(755, 500));
        _finalScrollPanel.setBorder(new EmptyBorder(0, 0,7,0));

        jpFinalRouteContainer.setBackground(GUI.getWhite());
        jpFinalRouteContainer.setBorder(new EmptyBorder(30, 30, 30, 30));

        jpSelectAddressesContainer.setBackground(GUI.getWhite());
        addressContainer.setBackground(GUI.getWhite());

        jcbAddressDropdown = new JComboBox();

        dbconnection connection = new dbconnection();
        ResultSet retourProductStateData = connection.executeQuery("SELECT distinct(destinationdate) as destinationdate FROM v_ng_destinationlist");
        boolean dataAvaliable = false;
        while(retourProductStateData.next()) {
            jcbAddressDropdown.addItem(retourProductStateData.getString("destinationdate"));
            dataAvaliable = true;
        }
        if (!dataAvaliable) {jcbAddressDropdown.addItem("Geen datum");}
        connection.closeConnection();

        jcbAddressDropdown.setPreferredSize(new Dimension(130, 30));
        jcbAddressDropdown.setBackground(GUI.getWhite());
        jcbAddressDropdown.setFont(GUI.getArial());
        jcbAddressDropdown.setBorder(BorderFactory.createLineBorder(GUI.getBlue()));

        Button retrieve = new Button("Ophalen adressen");
        jpRetrieveAdresses = retrieve.getButton();
        jpRetrieveAdresses.addActionListener(this);

        jlAddressesLabel.setFont(GUI.getArialBold().deriveFont(16f));

        addressContainer.setLayout(new BoxLayout(addressContainer, BoxLayout.Y_AXIS));

        Button calculate = new Button("Bereken route");
        jbCalculate = calculate.getButton();
        jbCalculate.addActionListener(this);

        jpNewCalculateContainer.setBackground(GUI.getWhite());
        jpCalculateContainer.setBackground(GUI.getWhite());

        // Route result
        jpOrderedAddressesContainer.setBackground(GUI.getWhite());

        jlCalcRouteLabel.setFont(GUI.getArialBold().deriveFont(16f));

        Button newCalc = new Button("Nieuwe route berekenen");
        jbNewCalculate = newCalc.getButton();
        jbNewCalculate.addActionListener(this);

        routeAddresses.setBackground(GUI.getWhite());
        routeAddresses.setLayout(new BoxLayout(routeAddresses, BoxLayout.Y_AXIS));

        jpSelectAddressesContainer.add(jcbAddressDropdown);
        jpSelectAddressesContainer.add(Box.createHorizontalStrut(20));
        jpSelectAddressesContainer.add(jpRetrieveAdresses);

        jpCalculateContainer.add(jbCalculate);

        jpOrderedAddressesContainer.add(jlCalcRouteLabel);
        jpNewCalculateContainer.add(jbNewCalculate);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        gbc.gridx = 0;

        jpRoute.add(jpSelectAddressesContainer, gbc);
        jpRoute.add(Box.createVerticalStrut(20), gbc);
        jpRoute.add(jlAddressesLabel, gbc);
        jpRoute.add(addressContainer, gbc);
        jpRoute.add(Box.createVerticalStrut(20), gbc);
        jpRoute.add(jpCalculateContainer, gbc);

        jpFinalRouteContainer.add(jpOrderedAddressesContainer, gbc);
        jpFinalRouteContainer.add(Box.createVerticalStrut(20), gbc);
        jpFinalRouteContainer.add(routeAddresses, gbc);
        jpFinalRouteContainer.add(Box.createVerticalStrut(20), gbc);
        jpFinalRouteContainer.add(jpNewCalculateContainer, gbc);

        /* --- GridBag hack to align content in the top left corner --- */
        gbc.weightx = 1;
        gbc.weighty = 1;
        jpRoute.add(new JLabel(""), gbc);
        jpFinalRouteContainer.add(new JLabel(""), gbc);

        _jpRouteScrollContainer.add(_scrollPanel);
        _jpRouteScrollContainer.add(_finalScrollPanel);
    }

    public void getAddresses(String type, String name, String fullAddress) {
        JLabel address = new JLabel(type + " - " + name + ": " + fullAddress);

        address.setFont(GUI.getArial());

        addressContainer.add(address);
    }

    public JPanel getRoute() {
        return _jpRouteScrollContainer;
    }

    public void show() {
        _jpRouteScrollContainer.setVisible(true);
        _scrollPanel.setVisible(true);
        jpRoute.setVisible(true);
    }

    public void hide() {
        _jpRouteScrollContainer.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jpRetrieveAdresses) {
            addressContainer.removeAll();
            addressContainer.revalidate();

            try {
                retrieveAddresses((String) jcbAddressDropdown.getSelectedItem());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else if (e.getSource() == jbCalculate) {
            System.out.println((String) jcbAddressDropdown.getSelectedItem());
            var result = GoogleRequest.getMap((String) jcbAddressDropdown.getSelectedItem());
            System.out.println(result);

            for (var entry : result.entrySet()) {
                final int volgorde = entry.getKey();
                final Destination destination = entry.getValue();

                for (var id : destination.getId().split(",")) {
                    if (id.contains("R_")) {
                        JLabel test = new JLabel("Retour: " +id+ " --  "+ destination.getCustomerName() + " " +destination.getAddress());

                        test.setFont(GUI.getArial());

                        routeAddresses.add(test);
                    }
                    else {
                        JLabel test = new JLabel("Retour: " +id+ " --  "+ destination.getCustomerName() + " " +destination.getAddress());

                        test.setFont(GUI.getArial());
                        routeAddresses.add(test);
                    }
                }

            }

            jpRoute.setVisible(false);
            _scrollPanel.setVisible(false);
        } else if (e.getSource() == jbNewCalculate) {
            addressContainer.removeAll();
            addressContainer.revalidate();

            jpRoute.setVisible(true);
            _scrollPanel.setVisible(true);
        }

        _jpRouteScrollContainer.updateUI();
    }

    private void retrieveAddresses(String dDate) throws SQLException {
        dbconnection connection = new dbconnection();
        ResultSet addresData = connection.executeQuery( "select typedestination, invoicereturnid, customerid, customername, addressline1, addressline2, postalcode, cityname " +
                "from wideworldimporters.v_ng_destinationlist " +
                "where destinationdate = '"+ dDate + "' " +
                "order by cityname, customerid");

        while(addresData.next()) {
            //getAddresses("Bestelling", "Oliver Queen", "Steenbokstraat 29, 9742 TS Groningen");
            getAddresses(addresData.getString("typedestination"), addresData.getString("customername"), (addresData.getString("addressline1") + " " + addresData.getString("postalcode") + " " +
                    addresData.getString("cityname")));
        }
        connection.closeConnection();
    }
}
