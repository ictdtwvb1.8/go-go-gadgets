package GUI;

import dbconnection.dbconnection;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RetoursGUI implements ActionListener {
    static final private JPanel _jpRetoursScrollContainer = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));

    static final private JPanel jpRetoursContainer = new JPanel(new BorderLayout());
    static final private JScrollPane _scrollpanel = new JScrollPane(jpRetoursContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    static final private JPanel jpRetours = new JPanel();

    static final private JPanel jpRetoursDetail = new JPanel();
    final private JScrollPane _detailScrollpanel = new JScrollPane(jpRetoursContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    private static JLabel _retourImage;
    private static CustomImage _image;
    private static JLabel _productName;
    private static JLabel _orderDate;
    private static JButton _inspect;
    private static int _retourID;
    private static String _retourDate;
    private static String _retourStatus;

    static final private JPanel jpRetourID = new JPanel(new FlowLayout(FlowLayout.LEFT));
    static final private JLabel jlRetourIDLabel = new JLabel("Retour:");
    static final private JPanel jpRetourDate = new JPanel(new FlowLayout(FlowLayout.LEFT));
    static final private JLabel jlRetourDateLabel = new JLabel("Retourdatum:");
    static final private JPanel jpRetourState = new JPanel(new FlowLayout(FlowLayout.LEFT));
    static final private JLabel jlRetourStateLabel = new JLabel("Retourstatus:");
    static final private JPanel jpRetourProductsLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    static final private JPanel jpRetourProducts = new JPanel();
    static final private JLabel _retourIDValue = new JLabel();
    static final private JLabel _retourDateValue = new JLabel();
    static final private JLabel _retourStateValue = new JLabel();
    static final private JLabel _retourProductsLabel = new JLabel();
    static final private JPanel jpRetourProductsContainer = new JPanel();

    static final private JPanel jpRetoursDetailVertical = new JPanel(new FlowLayout(FlowLayout.LEFT));
    static final private JPanel jpRetoursDetailVerticalBox = new JPanel();
    private static JLabel _productImage;
    private static CustomImage _pImage;
    private static JLabel _retourProductNameLabel = new JLabel();
    private static JLabel _retourProductName = new JLabel();
    private static JLabel _retourProductAmountLabel = new JLabel();
    private static JLabel _retourProductAmount = new JLabel();

    public RetoursGUI() {
        _jpRetoursScrollContainer.setPreferredSize(new Dimension(570, 550));
        _jpRetoursScrollContainer.setBackground(GUI.getWhite());

        _scrollpanel.setPreferredSize(new Dimension(566, 493));
        _scrollpanel.setBackground(GUI.getWhite());
        _scrollpanel.setBorder(null);

        jpRetours.setPreferredSize(new Dimension(570, 550));
        jpRetours.setLayout(new BoxLayout(jpRetours, BoxLayout.Y_AXIS));
        jpRetours.setBorder(new EmptyBorder(30, 30, 30, 30));
        jpRetours.setBackground(GUI.getWhite());

        _detailScrollpanel.setPreferredSize(new Dimension(566, 493));
        _detailScrollpanel.setBorder(null);

        jpRetoursDetail.setPreferredSize(new Dimension(570, 550));
        jpRetoursDetail.setLayout(new BoxLayout(jpRetoursDetail, BoxLayout.Y_AXIS));
        jpRetoursDetail.setBorder(new EmptyBorder(30, 30, 30, 30));
        jpRetoursDetail.setBackground(GUI.getWhite());

        jpRetourID.setBackground(GUI.getWhite());
        jpRetourDate.setBackground(GUI.getWhite());
        jpRetourState.setBackground(GUI.getWhite());
        jpRetourProductsLabel.setBackground(GUI.getWhite());

        jpRetoursContainer.add(jpRetours);

        jpRetoursContainer.setPreferredSize(new Dimension(570, jpRetours.getComponentCount() * 100));

        _jpRetoursScrollContainer.add(_detailScrollpanel);
    }

    public static void getRetours() throws SQLException {
        jpRetours.removeAll();
        jpRetours.revalidate();

        dbconnection connection = new dbconnection();
        ResultSet retourData = connection.executeQuery("select returnid, returndate, returnprogress, stockmedia from wideworldimporters.v_ng_returns where customerid ="+GUI.getCustomerID());
        while(retourData.next()) {
            _retourID = retourData.getInt("returnid");
            _retourDate = retourData.getString("returndate");
            _retourStatus = retourData.getString("returnprogress");
            String stockmedia = retourData.getString("stockmedia");
            jpRetours.add(setRetour(stockmedia, Integer.toString(_retourID), _retourDate, _retourStatus));
        }
        connection.closeConnection();
    }

    public static JPanel setRetour(String img, String rRetourID, String rDate, String _rState) {
        JPanel retour = new JPanel(new BorderLayout());
        JPanel retourText = new JPanel();
        JPanel btnContainer = new JPanel();

        retour.setMaximumSize(new Dimension(570, 95)); // Requested height + empty border bottom because JAVA

        retourText.setLayout(new BoxLayout(retourText, BoxLayout.Y_AXIS));
        btnContainer.setLayout(new BoxLayout(btnContainer, BoxLayout.Y_AXIS));

        retour.setBorder(new CompoundBorder(
                new EmptyBorder(0, 0, 20, 0), // Separates retours
                BorderFactory.createLineBorder(GUI.getBlue())));
        retourText.setBorder(new EmptyBorder(0, 10, 0, 10));
        btnContainer.setBorder(new EmptyBorder(0, 10, 0, 10));

        retour.setBackground(GUI.getWhite());
        retourText.setBackground(GUI.getWhite());
        retourText.setBackground(GUI.getWhite());
        btnContainer.setBackground(GUI.getWhite());

        _image = new CustomImage(img, 115, 75);
        _retourImage = new JLabel(new ImageIcon(_image.getImage()));

        _productName = new JLabel(rRetourID +" - Status: " + _rState);
        _orderDate = new JLabel(rDate);

        _productName.setFont(GUI.getArial()); // Sets font and font size
        _orderDate.setFont(GUI.getArial()); // Sets font and font size

        Button inspect = new Button("Bekijk retour");
        _inspect = inspect.getButton();
        _inspect.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                   _scrollpanel.setVisible(false);

                   jpRetourProductsContainer.removeAll();
                   jpRetourProductsContainer.revalidate();

                   jpRetours.setVisible(false); // Hide retours to reveal retourDetail (works better than to hide retours and than show retourdetail)
                   _retourID = Integer.parseInt(rRetourID);
                   _retourDate = rDate;
                   _retourStatus =_rState;

                   try {
                       getRetourDetail();
                   } catch (SQLException throwables) {
                       throwables.printStackTrace();
                   }

                   _jpRetoursScrollContainer.updateUI();
               }
           }
        );

        retourText.add(Box.createVerticalGlue());
        retourText.add(_productName);
        retourText.add(Box.createGlue());
        retourText.add(_orderDate);
        retourText.add(Box.createGlue());

        btnContainer.add(Box.createVerticalGlue());
        btnContainer.add(_inspect);
        btnContainer.add(Box.createGlue());

        retour.add(_retourImage, BorderLayout.LINE_START);
        retour.add(retourText, BorderLayout.CENTER);
        retour.add(btnContainer, BorderLayout.LINE_END);

        retour.updateUI();
        return retour;
    }

    private static void getRetourDetail() throws SQLException {
        jpRetoursDetailVerticalBox.setLayout(new BoxLayout(jpRetoursDetailVerticalBox, BoxLayout.Y_AXIS));
        jpRetourProductsContainer.setLayout(new BoxLayout(jpRetourProductsContainer, BoxLayout.Y_AXIS));

        jpRetourProducts.setBackground(GUI.getWhite());
        jpRetoursDetailVertical.setBackground(GUI.getWhite());

        jpRetoursDetailVertical.setBorder(new EmptyBorder(-5, -5, -5, -5));

        _retourIDValue.setText(Integer.toString(_retourID));
        _retourDateValue.setText(_retourDate);
        _retourStateValue.setText(_retourStatus);
        _retourProductsLabel.setText("Producten:");

        jlRetourIDLabel.setFont(GUI.getArialBold());
        jlRetourDateLabel.setFont(GUI.getArialBold());
        jlRetourStateLabel.setFont(GUI.getArialBold());
        _retourProductsLabel.setFont(GUI.getArialBold());

        _retourIDValue.setFont(GUI.getArial());
        _retourDateValue.setFont(GUI.getArial());
        _retourStateValue.setFont(GUI.getArial());

        jpRetourID.add(jlRetourIDLabel);
        jpRetourID.add(_retourIDValue);

        jpRetourDate.add(jlRetourDateLabel);
        jpRetourDate.add(_retourDateValue);

        jpRetourState.add(jlRetourStateLabel);
        jpRetourState.add(_retourStateValue);

        jpRetourProductsLabel.add(_retourProductsLabel);

        jpRetourProducts.add(getRetourProducts());

        jpRetoursDetailVerticalBox.add(jpRetourID);
        jpRetoursDetailVerticalBox.add(jpRetourDate);
        jpRetoursDetailVerticalBox.add(jpRetourState);
        jpRetoursDetailVerticalBox.add(jpRetourProductsLabel);
        jpRetoursDetailVerticalBox.add(jpRetourProducts);

        jpRetoursDetailVertical.add(jpRetoursDetailVerticalBox);

        jpRetoursDetail.add(jpRetoursDetailVertical);

        jpRetoursContainer.add(jpRetoursDetail);

        jpRetoursContainer.setPreferredSize(new Dimension(570, jpRetourProductsContainer.getComponentCount() * 90 + 200));
    }

    public static JPanel getRetourProducts() throws SQLException {
        dbconnection connection = new dbconnection();

        ResultSet retourProductsData = connection.executeQuery( "select count(stockitemid) as quantity, stockitemdescription, stockmedia " +
                                                                "from wideworldimporters.v_ng_returnitems " +
                                                                "where returnid = "+ _retourID + " " +
                                                                "group by returnid, stockitemid, stockitemdescription, stockmedia");
        while(retourProductsData.next()) {
            JPanel jpRetourProduct = new JPanel(new BorderLayout());
            JPanel jpRetourProductText = new JPanel();
            JPanel jpRetourProductNameContainer = new JPanel(new FlowLayout(FlowLayout.LEFT));
            JPanel jpRetourProductAmountContainer = new JPanel(new FlowLayout(FlowLayout.LEFT));

            jpRetourProductText.setLayout(new BoxLayout(jpRetourProductText, BoxLayout.Y_AXIS));

            jpRetourProduct.setBackground(GUI.getWhite());
            jpRetourProductText.setBackground(GUI.getWhite());
            jpRetourProductNameContainer.setBackground(GUI.getWhite());
            jpRetourProductAmountContainer.setBackground(GUI.getWhite());

            jpRetourProduct.setBorder(new CompoundBorder(
                    new EmptyBorder(0, 0, 20, 0), // Separates products
                    BorderFactory.createLineBorder(GUI.getBlue())));

            _pImage = new CustomImage(retourProductsData.getString("stockmedia"), 92, 70);
            _productImage = new JLabel(new ImageIcon(_pImage.getImage()));
            _retourProductAmount = new JLabel(Integer.toString(retourProductsData.getInt("quantity")));
            _retourProductName = new JLabel(retourProductsData.getString("stockitemdescription"));

            _retourProductNameLabel = new JLabel("Naam: ");
            _retourProductAmountLabel = new JLabel("Aantal: ");

            _retourProductNameLabel.setFont(GUI.getArialBold());
            _retourProductName.setFont(GUI.getArial());
            _retourProductAmountLabel.setFont(GUI.getArialBold());
            _retourProductAmount.setFont(GUI.getArial());

            jpRetourProductNameContainer.add(_retourProductNameLabel);
            jpRetourProductNameContainer.add(_retourProductName);

            jpRetourProductAmountContainer.add(_retourProductAmountLabel);
            jpRetourProductAmountContainer.add(_retourProductAmount);

            jpRetourProductText.add(Box.createVerticalGlue());
            jpRetourProductText.add(jpRetourProductNameContainer);
            jpRetourProductText.add(jpRetourProductAmountContainer);

            jpRetourProduct.add(_productImage, BorderLayout.LINE_START);
            jpRetourProduct.add(jpRetourProductText, BorderLayout.CENTER);

            jpRetourProductsContainer.add(jpRetourProduct);
        }
        connection.closeConnection();

        return jpRetourProductsContainer;
    }

    public JPanel getRetoursContainer() {
        return _jpRetoursScrollContainer;
    }

    public void show() {
        _jpRetoursScrollContainer.setVisible(true);
        _scrollpanel.setVisible(true);
        jpRetours.setVisible(true);
    }

    public void hide() {
        _jpRetoursScrollContainer.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
