package GUI;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CustomImage {
    private BufferedImage _image;

    public CustomImage(String image, int width, int height) {
        try {
            _image = ImageIO.read(new File(image));
        } catch (IOException e) {
            e.printStackTrace();
        }

        _image = rescaleImage(_image, width, height); // Resizes image based on width and height it cuts parts of the image from the center
    }

    private BufferedImage rescaleImage(BufferedImage src, int boxWidth, int boxHeight){
        int original_width = src.getWidth();
        int original_height = src.getHeight();
        int new_width;
        int new_height;
        int cutWidth = 0;
        int cutHeight = 0;
        int croppedWidth;
        int croppedHeight;

        if (original_width > original_height) { // Checks if the image is landscape or portrait
            new_width = (boxHeight * original_width) / original_height; // Scales width based on height while keeping the aspect ratio
            new_height = boxHeight;

            cutWidth = (new_width - boxWidth) / 2; // Defines the extra space that needs to be cut of the width
            croppedWidth = new_width - cutWidth - cutWidth; // Cuts of extra space
            croppedHeight = new_height;
        } else {
            new_width = boxWidth;
            new_height = (boxWidth * original_height) / original_width;

            cutHeight = (new_height - boxHeight) / 2;
            croppedWidth = new_width;
            croppedHeight = new_height - cutHeight - cutHeight;
        }

        Image tmp = src.getScaledInstance(new_width, new_height, Image.SCALE_SMOOTH);

        Graphics2D g2d = src.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        BufferedImage croppedImage = src.getSubimage(
                cutWidth, // Top right or coordinates where to cut image horizontal
                cutHeight, // Top right or coordinates where to cut image vertical
                croppedWidth, // width
                croppedHeight // height
        );

        return croppedImage;
    }

    public BufferedImage getImage() {
        return _image;
    }
}
