package GUI;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class GUI extends JFrame implements ActionListener {
    static private int _customerID;
    static private int _InvoiceID;
    static private int _ReturnID;
    static private int _ReturnlineID;

    static final private JPanel jpHomeContainer = new JPanel();

    static final private JPanel jpGlobalContainer = new JPanel(new BorderLayout());
    final private JPanel jpSubContainer = new JPanel();
    final private JPanel jpOrdersContainer = new JPanel();
    final private JPanel jpRegistrationContainer = new JPanel(new BorderLayout());
    final private JPanel jpRouteContainer = new JPanel(new BorderLayout());

    final private JPanel jpMenu = new JPanel(new FlowLayout(FlowLayout.LEFT));
    final private JButton jbOrders = new JButton("Bestellingen");
    final private JButton jbRegistration = new JButton("Registratie");
    final private JButton jbRoute = new JButton("Route");
    final private JButton jbLogout = new JButton("Uitloggen");

    final private JPanel jpOdersSideMenuContainer = new JPanel(new BorderLayout());
    final private JPanel jpOdersSideMenu = new JPanel();
    final private JPanel jpOdersSideContainer = new JPanel();
    final private JPanel jpOdersSideBorder = new JPanel();

    final private JPanel jpRegistrationSideMenu = new JPanel(new BorderLayout());
    final private JPanel jpRegistrationMenu = new JPanel();
    final private JPanel jpRegistrationSideContainer = new JPanel();
    final private JPanel jpRegistrationSideBorder = new JPanel();

    final private DrawBox dbSideOrdersBox = new DrawBox(12, 12);
    final private JButton jbSideOrders = new JButton("Bestellingen");
    final private DrawBox dbSideRetoursBox = new DrawBox(12, 12);
    final private JButton jbSideRetours = new JButton("Retouren");

    final private DrawBox dbSideRegistrationRetourBox = new DrawBox(12, 12);
    final private JButton jbSideRegistrationRetour = new JButton("Retour");
    final private DrawBox dbSideRegistrationProductBox = new DrawBox(12, 12);
    final private JButton jbSideRegistrationProduct = new JButton("Product");

    private static BufferedImage favicon;
    static final private Color cBlue = new Color(0,51,102);
    static final private Color cWhite = new Color(255,255,255);
    static private Font fArial;
    static private Font fArialBold;

    final private HomeGUI jpHome = new HomeGUI();

    final private OrdersGUI orders = new OrdersGUI();
    final private RetoursGUI retours = new RetoursGUI();
    final private RetourRegistrationGUI retourRegistrations = new RetourRegistrationGUI();
    final private ProductRegistrationGUI productRegistrations = new ProductRegistrationGUI();
    final private RouteGUI route = new RouteGUI();

    static {
        try {
            favicon = ImageIO.read(new File("App/resources/favicon/gogogadgets-icon.png"));

            fArial = Font.createFont(Font.TRUETYPE_FONT, new File("App/resources/fonts/arial.ttf")).deriveFont(14f);
            fArialBold = Font.createFont(Font.TRUETYPE_FONT, new File("App/resources/fonts/arial-bold.ttf")).deriveFont(14f);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("App/resources/fonts/arial.ttf")));
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("App/resources/fonts/arial-bold.ttf")));
        } catch (IOException | FontFormatException e) {
            e.printStackTrace();

        }
    }

    final private Font fMenu = fArial.deriveFont(16f);
    final private Font fMenuBold = fArialBold.deriveFont(16f);

    public GUI() throws SQLException {
        setTitle("Go Go Gadgets");
        setSize(750, 550); // Needed to open in middle of screen
        setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        setLocationRelativeTo(null); // Opens app in middle of screen
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setIconImage(favicon); // Sets top left icon

        jpHomeContainer.setPreferredSize(new Dimension(750, 550));
        jpHomeContainer.setBackground(cWhite);

        jpGlobalContainer.setPreferredSize(new Dimension(750, 550));
        jpGlobalContainer.setBackground(Color.cyan);
        jpGlobalContainer.setBorder(new EmptyBorder(-5, -5, -5, -5));

        setMenu();

        jpSubContainer.setLayout(new BoxLayout(jpSubContainer, BoxLayout.X_AXIS)); // Removed padding from jpSubContainer

        jpOrdersContainer.setLayout(new BoxLayout(jpOrdersContainer, BoxLayout.X_AXIS));
        jpOrdersContainer.setBackground(cWhite);

        jpRegistrationContainer.setLayout(new BoxLayout(jpRegistrationContainer, BoxLayout.X_AXIS));
        jpRegistrationContainer.setBackground(cWhite);

        // Home
        JScrollPane homePanel = jpHome.getHome();
        jpHomeContainer.add(homePanel);

        // Orders
        setSideMenu(jpOdersSideMenuContainer, jpOdersSideMenu, jpOdersSideContainer, jpOdersSideBorder, dbSideOrdersBox, jbSideOrders);
        setSideMenu(jpOdersSideMenuContainer, jpOdersSideMenu, jpOdersSideContainer, jpOdersSideBorder, dbSideRetoursBox, jbSideRetours);
        jpOrdersContainer.add(jpOdersSideMenuContainer);

        JScrollPane ordersPanel = orders.getOrdersContainer();
        jpOrdersContainer.add(ordersPanel);

        // Retours
        JPanel retoursPanel = retours.getRetoursContainer();
        retours.hide();
        jpOrdersContainer.add(retoursPanel);
        jpOrdersContainer.setVisible(false);

        // Registration
        setSideMenu(jpRegistrationSideMenu, jpRegistrationMenu, jpRegistrationSideContainer, jpRegistrationSideBorder, dbSideRegistrationRetourBox, jbSideRegistrationRetour);
        setSideMenu(jpRegistrationSideMenu, jpRegistrationMenu, jpRegistrationSideContainer, jpRegistrationSideBorder, dbSideRegistrationProductBox, jbSideRegistrationProduct);
        jpRegistrationContainer.add(jpRegistrationSideMenu);

        JScrollPane retourRegistrationsPanel = retourRegistrations.getRetourRegistration();
        jpRegistrationContainer.add(retourRegistrationsPanel);

        JScrollPane productRegistrationsPanel = productRegistrations.getProductRegistration();
        jpRegistrationContainer.add(productRegistrationsPanel);
        jpRegistrationContainer.setVisible(false);

        // Route
        JPanel routePanel = route.getRoute();
        jpRouteContainer.add(routePanel);
        jpRouteContainer.setVisible(false);

        jpSubContainer.add(jpOrdersContainer);
        jpSubContainer.add(jpRegistrationContainer);
        jpSubContainer.add(jpRouteContainer);

        jpGlobalContainer.add(jpMenu, BorderLayout.PAGE_START);
        jpGlobalContainer.add(jpSubContainer, BorderLayout.CENTER);

        add(jpHomeContainer);
        add(jpGlobalContainer);
        jpGlobalContainer.setVisible(false);

        pack();
        setVisible(true);
    }

    public static void logout() {
        jpHomeContainer.setVisible(true);
        jpGlobalContainer.setVisible(false);
    }

    public static void login() {
        jpHomeContainer.setVisible(false);
        jpGlobalContainer.setVisible(true);
    }

    public static Color getBlue() {
        return cBlue;
    }

    public static Color getWhite() {
        return cWhite;
    }

    public static Font getArial() {
        return fArial;
    }

    public static Font getArialBold() {
        return fArialBold;
    }

    public void setMenu() {
        jpMenu.setSize(750, 57);
        jpMenu.setBackground(cBlue);
        jpMenu.setBorder(new EmptyBorder(0, 0, -5, 0));

        setMenuStyle(jbOrders);
        setMenuStyle(jbRegistration);
        setMenuStyle(jbRoute);
        setMenuStyle(jbLogout);
    }

    public void setMenuStyle(JButton btn) {
        btn.setBorderPainted(false);
        btn.setFocusPainted(false);
        btn.setContentAreaFilled(false);

        btn.setPreferredSize(new Dimension(160, 57));
        btn.setForeground(cWhite);
        btn.setFont(fMenu);
        btn.setCursor(new Cursor(Cursor.HAND_CURSOR));

        btn.addActionListener(this);

        jpMenu.add(btn);
    }

    public void setSideMenu(JPanel sideMenuContainer, JPanel sideMenu, JPanel itemContainer, JPanel sideMenuBorder, DrawBox box, JButton btn) {
        sideMenuContainer.setPreferredSize(new Dimension(180, 550));
        sideMenuContainer.setBackground(cWhite);
        sideMenuContainer.setBorder(new EmptyBorder(30, 30, 30, 0));

        sideMenu.setLayout(new GridBagLayout());
        sideMenu.setBackground(cWhite);

        GridBagConstraints location = new GridBagConstraints();
        location.anchor = GridBagConstraints.NORTH;
        location.weightx = 1;
        location.weighty = 1;

        itemContainer.setLayout(new BoxLayout(itemContainer, BoxLayout.Y_AXIS));
        itemContainer.setBackground(cWhite);

        setSideMenuItems(itemContainer, box, btn);

        sideMenuBorder.setBackground(cWhite);
        sideMenuBorder.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, cBlue));

        sideMenu.add(itemContainer, location);

        sideMenuContainer.add(sideMenu, BorderLayout.LINE_START);
        sideMenuContainer.add(sideMenuBorder, BorderLayout.CENTER);
    }

    public void setSideMenuItems(JPanel panel, DrawBox box, JButton btn) {
        JPanel item = new JPanel(new FlowLayout(FlowLayout.LEFT));
        item.setBackground(cWhite);

        btn.setBorderPainted(false); // Removes border
        btn.setFocusPainted(false); // Removes focus style
        btn.setContentAreaFilled(false); // Removes background
        btn.setMargin(new Insets(0, 0, 0, 0)); // Removes space between text and border
        btn.setPreferredSize(new Dimension(110, 20)); // Sets a width against bold text
        btn.setHorizontalAlignment(SwingConstants.LEFT); // Aligns text left
        btn.setFont(fMenu);
        btn.setCursor(new Cursor(Cursor.HAND_CURSOR));

        btn.addActionListener(this);

        item.add(box);
        item.add(btn);

        panel.add(item);
    }

    public void resetGlobalContainer() {
        jpOrdersContainer.setVisible(false);
        orders.hide();
        retours.hide();

        jpRegistrationContainer.setVisible(false);
        retourRegistrations.hide();
        productRegistrations.hide();

        jpRouteContainer.setVisible(false);

        jbOrders.setFont(fMenu);
        jbSideOrders.setFont(fMenu);
        jbRegistration.setFont(fMenu);
        jbRoute.setFont(fMenu);
        jbSideRetours.setFont(fMenu);
        jbSideRegistrationRetour.setFont(fMenu);
        jbSideRegistrationProduct.setFont(fMenu);

        dbSideOrdersBox.setBackground(cWhite);
        dbSideRetoursBox.setBackground(cWhite);
        dbSideRegistrationRetourBox.setBackground(cWhite);
        dbSideRegistrationProductBox.setBackground(cWhite);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        resetGlobalContainer();

        switch (e.getActionCommand()) {
            case "Bestellingen":
                jbOrders.setFont(fMenuBold);
                jbSideOrders.setFont(fMenuBold);
                dbSideOrdersBox.setBackground(cBlue);

                jpOrdersContainer.setVisible(true);
                orders.show();
                break;
            case "Registratie", "Retour":
                jbRegistration.setFont(fMenuBold);
                jbSideRegistrationRetour.setFont(fMenuBold);
                dbSideRegistrationRetourBox.setBackground(cBlue);

                jpRegistrationContainer.setVisible(true);
                retourRegistrations.show();
                break;
            case "Route":
                jbRoute.setFont(fMenuBold);
                jpRouteContainer.setVisible(true);
                route.show();
                break;
            case "Uitloggen":
                System.out.println("customerID="+GUI.getCustomerID());
                logout();
                break;
            case "Retouren":
                jbOrders.setFont(fMenuBold);
                jbSideRetours.setFont(fMenuBold);
                dbSideRetoursBox.setBackground(cBlue);

                jpOrdersContainer.setVisible(true);
                try {
                    RetoursGUI.getRetours();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                retours.show();
                break;
            case "Product":
                jbRegistration.setFont(fMenuBold);
                jbSideRegistrationProduct.setFont(fMenuBold);
                dbSideRegistrationProductBox.setBackground(cBlue);

                jpRegistrationContainer.setVisible(true);
                productRegistrations.show();
                break;
        }

        revalidate();
        repaint();
    }

    public static void setCustomerID(int customerID){
        _customerID = customerID;
    }

    public static int getCustomerID(){
        return _customerID;
    }
}
