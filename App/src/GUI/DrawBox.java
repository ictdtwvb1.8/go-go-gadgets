package GUI;

import javax.swing.*;
import java.awt.*;

public class DrawBox extends JPanel {
    final private int _width;
    final private int _height;

    public DrawBox(int width, int height) {
        _width = width;
        _height = height;
        setPreferredSize(new Dimension(_width, _height));
        setBackground(Color.white);
        setBorder(BorderFactory.createLineBorder(GUI.getBlue(), 2));
    }
}
