package GUI;

import dbconnection.dbconnection;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.Integer.parseInt;

public class OrdersGUI implements ActionListener {
    final private JPanel jpOrdersContainer = new JPanel(new BorderLayout());
    final private JScrollPane _scrollPanel = new JScrollPane(jpOrdersContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

    static final private JPanel jpContainer = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private static JLabel _productImage;
    private static CustomImage image;
    private static JLabel _productName;
    private static JLabel _orderDate;
    private static JLabel _alreadySent;
    private static JButton _retrieveButton;
    private static JButton _sendButton;

    public OrdersGUI() {
        _scrollPanel.setPreferredSize(new Dimension(570, 500));
        _scrollPanel.setBorder(new EmptyBorder(0, 0, 5, 4));

        jpContainer.setPreferredSize(new Dimension(570, 550));
        jpContainer.setBackground(GUI.getWhite());
        jpContainer.setBorder(new EmptyBorder(30, 30, 30, 30));

        jpOrdersContainer.add(jpContainer);

        int count = (jpContainer.getComponentCount() % 2);
        if (count > 0) {
            count = (jpContainer.getComponentCount() / 2) + 1;
        } else {
            count = (jpContainer.getComponentCount() / 2);
        }

        jpOrdersContainer.setPreferredSize(new Dimension(570, count * 195));
    }

    public static void getOrders() throws SQLException {
        jpContainer.removeAll();
        jpContainer.revalidate();

        dbconnection connection = new dbconnection();

        ResultSet ordersData = connection.executeQuery("select invoiceid, invoicedate, stockmedia, isreturn from wideworldimporters.v_ng_invoices where customerid ="+GUI.getCustomerID());
        while(ordersData.next()) {
            int invoiceid = ordersData.getInt("invoiceid");
            String invoicedate = ordersData.getString("invoicedate");
            String stockmedia = ordersData.getString("stockmedia");
            String isreturn = ordersData.getString("isreturn");
            jpContainer.add(setOrder(stockmedia, Integer.toString(invoiceid), invoicedate, isreturn));
        }
        connection.closeConnection();
    }

    public JScrollPane getOrdersContainer() {
        return _scrollPanel;
    }

    public static Component setOrder(String img, String oOrderID, String oDate, String oIsRetour) {
        JPanel orderSpacer = new JPanel();
        JPanel order = new JPanel();
        JPanel orderTop = new JPanel();
        JPanel orderImage = new JPanel();
        JPanel orderText = new JPanel();
        JPanel orderButtons = new JPanel();

        order.setPreferredSize(new Dimension(230, 180));
        orderTop.setPreferredSize(new Dimension(230, 90));
        orderText.setPreferredSize(new Dimension(113, 45)); // 113 because 230 / 2 - 2 (border)
        orderButtons.setPreferredSize(new Dimension(230, 90));

        order.setLayout(new BoxLayout(order, BoxLayout.Y_AXIS));
        orderTop.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        orderImage.setLayout(new FlowLayout(FlowLayout.LEFT,0,0));
        orderText.setLayout(new BorderLayout());
        orderButtons.setLayout(new BorderLayout());

        orderSpacer.setBorder(new EmptyBorder(-5, -5, -5, 15)); // Creates space between orders
        order.setBorder(BorderFactory.createLineBorder(GUI.getBlue()));
        orderTop.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, GUI.getBlue()));
        orderButtons.setBorder(new EmptyBorder(10, 10, 10, 10));

        orderSpacer.setBackground(GUI.getWhite());
        order.setBackground(GUI.getWhite());
        orderTop.setBackground(GUI.getWhite());
        orderText.setBackground(GUI.getWhite());
        orderButtons.setBackground(GUI.getWhite());

        image = new CustomImage(img, 115, 90);
        _productImage = new JLabel(new ImageIcon(image.getImage()));

        _productName = new JLabel(oOrderID, JLabel.CENTER);
        _orderDate = new JLabel(oDate, JLabel.CENTER);

        _productName.setFont(GUI.getArial());
        _orderDate.setFont(GUI.getArial());


        System.out.println("outside if -- Wat is de waarde "+"'"+oIsRetour+"'");

        if(oIsRetour.equals("Yes")){
            System.out.println("inside if -- Wat is de waarde "+"'"+oIsRetour+"'");

            _alreadySent = new JLabel("Is al aangemeld", JLabel.CENTER);
            orderText.add(_alreadySent, BorderLayout.PAGE_START);
            orderButtons.add(_alreadySent, BorderLayout.PAGE_START);

        }else if(oIsRetour.equals("No")){
            System.out.println("inside elseif -- Wat is de waarde "+"'"+oIsRetour+"'");
            Button retrieve = new Button("Op laten halen");
            _retrieveButton = retrieve.getButton();

            Button send = new Button("Opsturen via PostNL");
            _sendButton = send.getButton();

            _retrieveButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // Op laten halen
                    convertOrderToRetour(parseInt(oOrderID), 1);
                }
            });
            _sendButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    // Versturen via PostNL
                    convertOrderToRetour(parseInt(oOrderID), 1);
                }
            });
            orderButtons.add(_retrieveButton, BorderLayout.PAGE_START);
            orderButtons.add(_sendButton, BorderLayout.PAGE_END);

        }



        orderImage.add(_productImage);
        orderText.add(_productName, BorderLayout.PAGE_START);
        orderText.add(_orderDate, BorderLayout.PAGE_END);


        orderTop.add(orderImage);
        orderTop.add(orderText);

        order.add(orderTop);
        order.add(orderButtons);

        orderSpacer.add(order);

        return orderSpacer;
    }

    public void show() {
        _scrollPanel.setVisible(true);
    }

    public void hide() {
        _scrollPanel.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
    private static void convertOrderToRetour(int orderid, int postalid){
        dbconnection connection = new dbconnection();
        // Parameter 1 = customerid, Parameter 2 = invoiceid, Parameter 3 = postalmethodid (1=ophalen,2=postnl punt), Parameter 4 = returndate
        connection.callProcedure("CALL WIDEWORLDIMPORTERS.RETURNINVOICE("+GUI.getCustomerID()+", "+orderid+", 2, now())");
        connection.closeConnection();
        JOptionPane.showMessageDialog(null, "Retourmelding is aangemaakt en wordt door u aangeboden bij een PostNL punt");
        try {
            getOrders();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
