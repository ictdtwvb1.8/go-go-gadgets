package GUI;

import dbconnection.dbconnection;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class RetourRegistrationGUI implements ActionListener {
    final private JPanel jpRegistrationContainer = new JPanel(new BorderLayout());
    final private JScrollPane _scrollPanel = new JScrollPane(jpRegistrationContainer, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    final private JPanel jpRetours = new JPanel();

    final private JLabel jlRetourStateLabel = new JLabel("Onbehandelde retour status aanpassen");

    static final private JPanel jpRetoursContainer = new JPanel();

    private static JLabel _retourImage;
    private static CustomImage _image;
    private static JLabel _productName;
    private static JLabel _productId;
    private static JLabel _orderDate;
    private static JComboBox _orderState;
    private static int _retourID;
    private static String _retourDate;
    private static String _retourStatus;

    private static int _hashKey = 0;
    static HashMap<String, JComboBox> _btnComboMap = new HashMap();

    private static JButton jlConfirmChanges;

    public RetourRegistrationGUI() throws SQLException {
        _scrollPanel.setPreferredSize(new Dimension(570, 500));
        _scrollPanel.setBorder(new EmptyBorder(0, 0,5,4));

        jpRetours.setPreferredSize(new Dimension(570, 550));
        jpRetours.setLayout(new FlowLayout(FlowLayout.LEFT));
        jpRetours.setBorder(new EmptyBorder(30, 30, 30, 30));
        jpRetours.setBackground(GUI.getWhite());

        jlRetourStateLabel.setFont(GUI.getArialBold().deriveFont(16f));
        jlRetourStateLabel.setBorder(new EmptyBorder(0, 0, 10, 0));

        jpRetoursContainer.setLayout(new BoxLayout(jpRetoursContainer, BoxLayout.Y_AXIS));

        jpRetours.add(jlRetourStateLabel);
        jpRetours.add(jpRetoursContainer);


        jpRegistrationContainer.add(jpRetours);

        jpRegistrationContainer.setPreferredSize(new Dimension(570, jpRetoursContainer.getComponentCount() * 100 + 150));
    }



    public static void getRegistrationRetours() throws SQLException {
        jpRetoursContainer.removeAll();
        jpRetoursContainer.revalidate();

        dbconnection connection = new dbconnection();
        ResultSet retourData = connection.executeQuery("select returnid, returndate, returnprogress, stockmedia from wideworldimporters.v_ng_returns where returnprogress = 'Aangemaakt'  and customerid ="+ GUI.getCustomerID());
        while(retourData.next()) {
            _retourID = retourData.getInt("returnid");
            _retourDate = retourData.getString("returndate");
            _retourStatus = retourData.getString("returnprogress");
            String stockmedia = retourData.getString("stockmedia");
            jpRetoursContainer.add(getRetour(retourData.getString("stockmedia"), "",retourData.getString("returnid"), retourData.getString("returndate")));
        }
        connection.closeConnection();
    }






    public static JPanel getRetour(String img, String name, String id, String date) throws SQLException {
        JPanel jpRetour = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        JPanel jpRetourBorder = new JPanel(new BorderLayout());
        JPanel jpRetourText = new JPanel();
        JPanel jpRetourState = new JPanel();

        jpRetour.setBackground(GUI.getWhite());
        jpRetour.setBorder(new EmptyBorder(0, 0, 10, 0));

        jpRetourBorder.setPreferredSize(new Dimension(500, 90));
        jpRetourBorder.setBorder(BorderFactory.createLineBorder(GUI.getBlue()));

        jpRetourText.setLayout(new BoxLayout(jpRetourText, BoxLayout.Y_AXIS));
        jpRetourState.setLayout(new BoxLayout(jpRetourState, BoxLayout.Y_AXIS));

        jpRetourText.setBackground(GUI.getWhite());
        jpRetourState.setBackground(GUI.getWhite());

        jpRetourText.setBorder(new EmptyBorder(0, 10, 0, 10));
        jpRetourState.setBorder(new EmptyBorder(0, 10, 0, 10));

        _image = new CustomImage(img, 110, 90);
        _retourImage = new JLabel(new ImageIcon(_image.getImage()));

        _productName = new JLabel(name);
        _productId = new JLabel(id);
        _orderDate = new JLabel(date);

        _orderState = new JComboBox();

        Button confirm = new Button("Aanpassingen doorvoeren");
        jlConfirmChanges = confirm.getButton();

        jlConfirmChanges.setName(String.valueOf(_retourID));
        _btnComboMap.put(jlConfirmChanges.getName(), _orderState);

        jlConfirmChanges.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String SelectedRetourStatus = _btnComboMap.get(((JComponent) e.getSource()).getName()).getSelectedItem().toString();
                String SelectedRetourID = ((JComponent) e.getSource()).getName();
                dbconnection connection = new dbconnection();
                connection.updateStatement(    "update wideworldimporters.returns " +
                                                    "set returnprogressid = (select returnprogressid from wideworldimporters.returnprogress where description = '"+ SelectedRetourStatus + "') "  +
                                                    "where returnid = "+SelectedRetourID);

                connection.closeConnection();

                //Alerts the user that the state has been changed
                JOptionPane.showMessageDialog(null, "Retourstatus is aangepast en zal niet meer worden weergegeven");
                try {
                    getRegistrationRetours();
                    RetoursGUI.getRetourProducts();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });


        dbconnection connection = new dbconnection();
        ResultSet retourStateData = connection.executeQuery("select description from wideworldimporters.returnprogress where description <> 'Aangemaakt'");

        while(retourStateData.next()) {
            _orderState.addItem(retourStateData.getString("description"));
        }
        connection.closeConnection();

        _productName.setFont(GUI.getArialBold());
        _productId.setFont(GUI.getArial());
        _orderDate.setFont(GUI.getArial());

        _orderState.setPreferredSize(new Dimension(130, 10));
        _orderState.setBackground(GUI.getWhite());
        _orderState.setFont(GUI.getArial());
        _orderState.setBorder(BorderFactory.createLineBorder(GUI.getBlue()));

        jpRetourText.add(Box.createVerticalGlue());
        jpRetourText.add(_productName);
        jpRetourText.add(Box.createGlue());
        jpRetourText.add(_productId);
        jpRetourText.add(Box.createGlue());
        jpRetourText.add(_orderDate);
        jpRetourText.add(Box.createGlue());

        jpRetourState.add(Box.createVerticalGlue());
        jpRetourState.add(_orderState);
        jpRetourState.add(Box.createGlue());
        jpRetourState.add(jlConfirmChanges);
        jpRetourState.add(Box.createGlue());

        jpRetourBorder.add(_retourImage, BorderLayout.LINE_START);
        jpRetourBorder.add(jpRetourText, BorderLayout.CENTER);
        jpRetourBorder.add(jpRetourState, BorderLayout.LINE_END);

        jpRetour.add(jpRetourBorder);

        _hashKey++;

        return jpRetour;
    }

    public JScrollPane getRetourRegistration() {
        return _scrollPanel;
    }

    public void show() {
        _scrollPanel.setVisible(true);
    }

    public void hide() {
        _scrollPanel.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == null) {

        }
    }
}
