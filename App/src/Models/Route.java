package Models;

import java.util.HashMap;
import java.util.Map;
// JsonObject Google API - Route => Row
public class Route {
    private String[] destination_addresses;
    private String[] origin_addresses;
    private Row[] rows;
    private String status;

    /*
        het ophalen van regels van de bestemmingen
    */
    public Element[] getElements(){
        try {
            return rows[0].getElements();
        }
        catch (Exception e){
            return null;
        }
    }

    /*
         het ophalen van alle afstanden
    */
    public Map<Integer, Integer> getDistances(){
        final Element[] elements = this.getElements();

        if(elements == null){
           return new HashMap<Integer, Integer>();
        }

        final Map<Integer, Integer> distances = new HashMap<>();
        int index = 0;

        for(Element element : elements){
            final Distance distance = element.getDistance();

            distances.put(index, distance.getValue());
            index++;
        }

        return distances;
    }

    /*
        het ophalen van status van de API call
    */
    public String getStatus(){
        return status;
    }
}
