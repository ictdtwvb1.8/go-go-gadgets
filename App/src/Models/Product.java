package Models;

import java.awt.image.BufferedImage;

public class Product {
    private int _productcode;
    private String _name;
    private BufferedImage _photo;

    public int getProductCode() {
        return _productcode;
    }

    public String getName() {
        return _name;
    }

    public BufferedImage getPhoto() {
        return _photo;
    }
}
