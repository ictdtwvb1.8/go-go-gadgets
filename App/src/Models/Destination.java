package Models;

import dbconnection.dbconnection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Destination {
    private static final Map<String, Destination> _destinations = new HashMap<>();
    private final String _id;
    private final int _customerId;
    private final String _customerName;
    private final String _address;
    private int _distanceFromDestionation = 0;

    public Destination(ResultSet destinations) throws SQLException {
        // data uit de database wordt omgezet naar een object
        _id = destinations.getString("InvoiceReturnID");
        _customerId = destinations.getInt("customerId");
        _customerName = destinations.getString("customername");
        _address = destinations.getString("addressline1") +
                    " "+
                    destinations.getString("postalcode") +
                    " " +
                    destinations.getString("cityname");

        // opslaan in een statsche lijst van bestemmingen
        _destinations.put(_id, this);
    }

    public static void setDestinations(){
        setDestinations("2021-05-04'");
    }

    public static void setDestinations(String date){
        final dbconnection connection = new dbconnection();

         try{
            final ResultSet destinationsData = connection
                    .executeQuery(  "select customerid, group_concat(InvoiceReturnID) as InvoiceReturnID, customername, cityname, postalcode, addressline1, destinationdate " +
                                    "from wideworldimporters.V_NG_destinationlist " +
                                    "where destinationdate =  '" + date + "' " +
                                    "group by 1,3,4,5,6,7 limit 25;");


            while(destinationsData.next()) {
                System.out.println(destinationsData.getString("InvoiceReturnID"));
                new Destination(destinationsData);
            }
         }
         catch(SQLException e){
            e.printStackTrace();
            System.out.println("setDestinations");
         }
         finally {
             connection.closeConnection();
         }
    }

    public static Map<String, Destination> getDestinations(){
        return _destinations;
    }

    /*
        het leeg maken van statsche data
     */
    public static void clearDestinations(){
        _destinations.clear();
    }

    /*
        het ophalen van een bestemming
    */
    public static Destination getDestinationById(String id){
        return _destinations.get(id);
    }

    /*
      het ophalen van een bestemming adres
    */
    public String getAddress(){
        return _address;
    }

    /*
      het ophalen van een bestemming klantnaam
    */
    public String getCustomerName(){
        return _customerName;
    }

    /*
      het ophalen van een bestemming id/id's
    */
    public String getId(){
        return _id;
    }

    /*
      het ophalen van de afstand van de bestemming
    */
    public int getDistanceFromDestionation(){
        return _distanceFromDestionation;
    }


    /*
      het updaten van de afstand van de bestemming
    */
    public void updateDistance(int distance){
        _distanceFromDestionation = distance;
    }
}
