package Models;

import java.util.HashMap;
import java.util.Map;

public class Customer {
    final private int _customerid;
    final private String _name;
    final private String _address;
    final private String _postcode;
    final private String _residence;
    final private static Map<Integer, Customer> _customers = new HashMap<>();

    public Customer(int customerid, String name, String address, String postcode, String residence) {
        _customerid = customerid;
        _name = name;
        _address = address;
        _postcode = postcode;
        _residence = residence;
    }

    public void setCustomer(int customerid, Customer customer) {
        _customers.put(customerid, customer);
    }

    public static Customer getCustomer(int customerid) {
        return _customers.get(customerid);
    }

    @Override
    public String toString() {
        return _customerid + ", " + _name + ", " + _address + ", " + _postcode + ", " + _residence;
    }

}
