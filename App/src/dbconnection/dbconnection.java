package dbconnection;

import java.sql.*;

public class dbconnection {
    final private String _url = "jdbc:mysql://localhost:3306/wideworldimporters";
    final private String _username = "root";
    final private String _password = "";
    private Connection _connection;

    public dbconnection()  {
        try {
            _connection = DriverManager.getConnection(_url, _username, _password);
        } catch (SQLException e) {
            System.out.println("Error");
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            Statement _statement = _connection.createStatement();
            return _statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void closeConnection(){
        try{
            _connection.close();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    // callProcedure
    public void callProcedure(String query) {
        try {
            Statement _statement = _connection.createStatement();
            _statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateStatement(String query) {
        try {
            Statement _statement = _connection.prepareStatement(query);
            _statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
